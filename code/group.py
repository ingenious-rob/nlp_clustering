# group quality code included here
from vector_math import *
import random


class Group:
  
  phrases = []
  average = []
  group_confidence = 0
  quality = "unmarked"
  best_phrase = None
  worst_phrase = None
  
  
  def __init__(self):
    self.phrases = []
    self.previous_size = 0
    self.size = 0
    self.recommended_training_phrases = []
    self.id = -1
    self.best_phrase = None
    self.worst_phrase = None
    self.all_keywords = {} 
    self.prototype = [0]*512

  def add_phrase(self, phrase):
    self.phrases.append(phrase)  

  def generate_prototype(self):
    self.prototype = []
    for i in range(0, 512):
      self.prototype.append(float(random.randrange(-1000000,1000000)/1000000))
    self.last_prototype = self.prototype

  def stable(self):
    if (round(euc_distance(self.prototype, self.last_prototype), 5) == (0.00000)):
      return True
    else:
      self.last_prototype = self.prototype
      return False

  def update_centre(self):
    self.size = len(self.phrases)
    #i in range 0 to 512
    if self.size == 0:
      self.prototype = self.prototype
    else:
      for i in range(0, len(self.prototype)):
        point_avg = 0

        #j in range prev size - current size, index of phrase
        for j in range(0, self.size):
          point_avg += self.phrases[j].vector[i]

        point_avg /= self.size
        self.prototype[i] = point_avg
      
    self.prototype = normalise(self.prototype)

  def update_average(self):
    self.size = len(self.phrases)

    if self.size !=0:
      #i in range 0 to 512
      for i in range(0, len(self.prototype)):
        point_avg = self.prototype[i]
        point_avg *= self.previous_size

        #j in range prev size - current size, index of phrase
        for j in range(self.previous_size, self.size):
          point_avg += self.phrases[j].vector[i]

        point_avg /= self.size
        self.prototype[i] = point_avg

    self.previous_size = self.size

  
  # similarity between each phrase and the average
  def update_phrase_confidence(self):
    for phrase in self.phrases:
      phrase.matching_confidence = similarity(phrase.vector, self.prototype)
      
  # gets the similarity between the average and the outermost or "worst" point
  def get_limit(self):
    lim = 1
    for phrase in self.phrases:
      if similarity(phrase.vector, self.average) < lim:
        lim = similarity(phrase.vector, self.prototype)
      
    return lim
  
  def assign_id(self, new_id):
    self.id = new_id
    for phrase in self.phrases:
        phrase.give_id(self.id)
  
  # get the best (closest to average) and worst (further from average) phrases
  def find_best_worst(self):
    
    # values initialised to opposite extremes
    best_phrase_value = 0
    worst_phrase_value = 2
    
    self.update_phrase_confidence()
    
    for i, phrase in enumerate(self.phrases):
      sim = phrase.matching_confidence
      
      if (self.best_phrase==None):
        self.best_phrase = phrase
        self.worst_phrase = phrase
      
      if (sim>self.best_phrase.matching_confidence):
        self.best_phrase = phrase

        
      if (sim<self.worst_phrase.matching_confidence):
        self.worst_phrase = phrase

    try:
      self.best = self.best_phrase.matching_confidence
    except:
      self.best = 0
    
    try:
      self.worst = self.worst_phrase.matching_confidence
    except:
      self.worst = 0
  
  # return the total number of intents in the group
  def count_intents(self):
    return len(list(self.get_intent_frequencies().keys()))
  
  # count the number of times each intent appears
  def get_intent_frequencies(self):
    frequency = {}
    
    for phrase in self.phrases:
      if list(frequency.keys()).count(phrase.passage_title)==0:
        frequency[str(phrase.passage_title)] = int(1)
      
      else:
        frequency[phrase.passage_title] += 1
    
    return frequency
   
  # get the intent that has the majority
  # split majorities return the first one found
  def get_dominant_intent(self):
    max_freq = 0
    result = None
    for intent, frequency in self.get_intent_frequencies().items():
      if frequency > max_freq:
        max_freq = frequency
        result = intent 
    
    return str(result)
  
  # return all the phrases matching an intent within the group
  def get_phrases_for_intent(self, intent):
    result = []
    
    for phrase in self.phrases:
      if phrase.passage_title == intent:
        result.append(phrase)
    
    return result

  # determine the quality of the group based on multiple factors
  # this functionality may have to be split off later
  def run_quality_analysis(self, new_id, algorithm_id):
    self.assign_id(new_id)
    self.algorithm_id = algorithm_id
    self.find_best_worst()
    
    quality_value = 0
    metric_count = 3

    self.intent_count = self.count_intents()    
    self.dominant = self.get_dominant_intent()
    self.correct = len(self.get_phrases_for_intent(self.dominant))
    try:
      self.precision = self.correct/len(self.phrases)
    except:
      self.precision = 0