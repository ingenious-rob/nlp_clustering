import json
import copy
import csv
import boto3
import os
import time
import collections
from group import Group
from phrase import Phrase
from string import Template

#NOTE: Project version
def obj_dict(obj):
  if isinstance(obj, Phrase):
    k = copy.deepcopy(obj.__dict__)
    k.pop("vector")
    k.pop("grouped")
    return k

  if isinstance(obj, Group):
    k = copy.deepcopy(obj.__dict__)
    k.pop("average")
    return k
      
    return obj.__dict__


def save_json(phrase_group_map, f_name):
  content = json.dumps(phrase_group_map, default=obj_dict)
  f_name = f_name + '.json'
      
  with open(f_name, 'w') as file:
    file.write(content)
    file.close()

def save_csv(phrase_group_map, phrases, f_name):
  f_name = f_name + ".csv"
  with open(f_name, mode='w', encoding = "utf-8", errors = "replace") as csv_file:
    fieldnames = []

    '''fieldnames = list(phrase_group_map[0].phrases[0].__dict__.keys())
    for field in phrases[0].other_data.keys():
      fieldnames.append(field)
    print(fieldnames)
    fieldnames.remove("other_data")'''
    for key in ("group_id","text", "vector","passage_title", "time", "confidence","handled"):
      fieldnames.append(key)

    writer = csv.DictWriter(csv_file, fieldnames=fieldnames,delimiter=',', lineterminator='\n')

    for phrase in phrases:
      to_write = copy.deepcopy(phrase.__dict__)
      to_write.pop("other_data")
      to_write.update(phrase.other_data)
      to_write = collections.OrderedDict(to_write)

      data = {key: value for key, value in to_write.items()
        if key in fieldnames}
      writer.writerow(data)

    csv_file.close()

def save_group(phrase_group_map, phrases, f_name):
  f_name = f_name + ".csv"

  with open(f_name, mode='a', encoding = "utf-8", errors = "replace") as csv_file:
    fieldnames = []

    for key in ("id","intent_count","precision","prototype","best", "worst", "size","dominant"):
      fieldnames.append(key)

    writer = csv.DictWriter(csv_file, fieldnames=fieldnames,delimiter=',', lineterminator='\n')

    for group in phrase_group_map:
      to_write = copy.deepcopy(group.__dict__)
      to_write = collections.OrderedDict(to_write)

      data = {key: value for key, value in to_write.items()
        if key in fieldnames}
      writer.writerow(data)

    csv_file.close()

def save_master(to_write, f_name):
  f_name = f_name + ".csv"

  with open(f_name, mode='a', encoding = "utf-8", errors = "replace") as csv_file:
    fieldnames = []

    for key in ("algorithm id","time","accuracy","number of groups","settings"):
      fieldnames.append(key)

    writer = csv.DictWriter(csv_file, fieldnames=fieldnames,delimiter=',', lineterminator='\n')
    
    data = {key : value for key, value in to_write.items()
      if key in fieldnames}

    writer.writerow(data)

  csv_file.close()


