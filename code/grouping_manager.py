import tensorflow as tf
import tensorflow_hub as hub
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
import re
import seaborn as sns
import math
import io
import sys
import nlp_grouping_algorithm as grouping
import sys, getopt
import boto3
import json
import time

from pipeline import Pipeline
from writer import *
from phrase import Phrase
from random import shuffle
from print_analysis import *

def setup_phrase_objects(dataframe):
  start = 0
  end = len(dataframe["question"])

  phrases = []

  for i in range(start, end):

    other_data = {}

    for col in dataframe.columns:
      if (col.lower()!="question") and (col!="Correct Passage Title") and (col.find("Unnamed")==-1) and (col!="passage_title") and (col.lower()!="vector"):
        if col.lower() == "time":
          other_data[str(col)] = str(time.strftime('%H:%M:%S %d-%m-%Y', time.localtime(int(dataframe[col][i]/1000) ) ) )
        else:
          other_data[str(col)] = str(dataframe[col][i])
    
    try:
      phrase_to_add = Phrase(str(dataframe["question"][i]),
        [float(i) for i in dataframe["vector"][i].replace('[','').replace(']','').split(",")],
        str(dataframe["Correct Passage Title"][i]))
    except:
      phrase_to_add = Phrase(str(dataframe["question"][i]),[float(i) for i in dataframe["vector"][i]],str(dataframe["passage_title"][i]))
    phrase_to_add.update_other_data(other_data)
    if len(phrase_to_add.other_data) == 0:
      print("There is no 'other data' present")

    phrases.append(phrase_to_add)

  return phrases

def run_embedding(inputs):
  #IMPORTANT
  #Universal Sentence Encoder is downloadable via: https://tfhub.dev/google/universal-sentence-encoder/2?tf-hub-format=compressed
  #After downloading, export and put in the folder "nlp/universal_sentence_encoder"

  #Get module path
  relative_path = os.path.abspath(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))
  module_path = os.path.join(relative_path, "universal_sentence_encoder_v2")
  timer = time.time()  
  print("Starting message embeddings")

  with tf.Graph().as_default():
    embed = hub.Module(module_path)

    messages = tf.placeholder(dtype=tf.string, shape = [None])
    output = embed(messages)

    with tf.Session() as session:
      session.run([tf.global_variables_initializer(), tf.tables_initializer()])
      message_embeddings = session.run(output, feed_dict={messages: inputs})
      print("Finished embedding in {} seconds".format(time.time() - timer))

  return message_embeddings

def setup_pipeline(config_file):
  if config_file == "none":
    pipeline = Pipeline(functions = [tuple(("trimming", {"maximum":30})), tuple(("match average", {"threshold" : 0.7}))])
    #pipeline = Pipeline(functions = [tuple(("trimming", {"maximum":30})), tuple(("spherical k-means", {"k" : 20, "max_iterations":100}))])
  else:
    with open(config_file) as f:
      data = json.load(f)
    list_of_tuples = []
    for func in data:
      list_of_tuples.append( tuple(( list(func.keys())[0], list(func.values())[0] )) )
      #print(list(func.keys())[0])
      #print(list(func.values())[0])
    pipeline = Pipeline(functions = list_of_tuples)

  return pipeline, list_of_tuples


def run(algorithm_id, environment, config_file, dataframe, output_name, skip_embeddings=True, email=None):
  print("Sample of size {}".format(len(dataframe["question"])))

  pipeline, config_tuples = setup_pipeline(config_file)

  if (environment == "local"):
    user_input = input("Run embeddings? ").lower()
    if (user_input=="yes") or (user_input=="y"):
      print("")
    else:
      return 0

  if skip_embeddings == False:
    inputs = dataframe["question"].tolist()
    message_embeddings = run_embedding(inputs)
    phrase_vectors = np.array(message_embeddings).tolist()
    #print(len(phrase_vectors))
    for i,v in enumerate(phrase_vectors):
      phrase_vectors[i] = grouping.normalise(v)
    dataframe['vector'] = pd.Series(phrase_vectors, index=dataframe.index)


  phrases = setup_phrase_objects(dataframe)

  intent_group = {}

  for phrase in phrases:
    intent_group[phrase.passage_title] = []

  for phrase in phrases:
    intent_group[phrase.passage_title].append(phrase)

  if (environment == "local"):
    user_input = input("Run grouping algorithm? ").lower()
    if (user_input=="y") or (user_input=="yes"):
      print("")
    else:
      return 0

  phrase_group_map = []
  grouping.reset_groups(phrase_group_map, phrases)

  print("Running grouping algorithm")
  t2 = time.time()
  pipeline.run(phrase_group_map, phrases)
  final_time = time.time()-t2
  #phrase_group_map = grouping.match_average(phrase_group_map, trimmed_phrases, threshold, True, True)
  print("Grouping finished in {} seconds".format(time.time()-t2))
  print("Running group quality analysis")
  #Groups needs to have quality analysis run before saving as this updates everything needed for the front end

  for i, group in enumerate(phrase_group_map):
    group.run_quality_analysis(i, algorithm_id)

  total_correct = 0
  for i, group in enumerate(phrase_group_map):
    total_correct += group.correct
  accuracy = total_correct/len(phrases)
  print("total correct: " + str(total_correct))
  print(str(len(phrases)))
  print(str(accuracy))

  if (environment == "local"):
    user_input = input("Save the file? ").lower()

  master_data = {
    "algorithm id":algorithm_id,
    "time":final_time, 
    "accuracy":accuracy, 
    "number of groups":len(phrase_group_map),
    "settings": config_tuples
  }

  if ("environment"!="local") or (user_input =="yes") or (user_input=="y"):
    if (environment == "local"):
      user_input = input("Would you like to save as a json, csv or both? ").lower()
    elif (environment == "project"):
      user_input = "csv"
    else:
      user_input = "both"

    if user_input=="json":
      print("Saving json...")
      save_json(phrase_group_map, output_name)
      
    elif user_input=="csv":
      print("Saving csv...")
      save_csv(phrase_group_map, phrases, output_name + "_" + algorithm_id)
      save_group(phrase_group_map, phrase, "groups")
      save_master(master_data, "master")