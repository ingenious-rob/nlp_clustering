#not ready for full implementation yet

import nlp_grouping_algorithm as grouping
from enum import Enum


def test_func(to_print):
  print(to_print)

#Function = namedtuple("Function", ["name", "parameters"])

fixed_models = {
  "default":{"trimming":None, "match average":None}

}

function_dict = {
  "test" : grouping.grouping_test, 
  "match average" : grouping.match_average, 
  "match first" : grouping.match_first, 
  "trimming" : grouping.trimming,
  "large group processing" : grouping.large_group_processing,
  "small group_processing" : grouping.small_group_processing,
  "group splitting" : grouping.group_splitting,
  "group realignment" : grouping.group_realignment,
  "spherical k-means" : grouping.spherical_k_means
}



class Pipeline():
  

  def __init__(self, model_type=None, functions=[]):
    print("Running pipeline version (WIP)")
    self.pipeline_queue = []
    self.trimmed_phrases = []
    self.trimmed = False

    if (model_type is None) and (len(functions)==0):
      self.queue_fixed("default")
    elif (model_type is not None):
      self.queue_fixed(model_type.lower())
    elif (model_type is None) and (len(functions)>0):
      self.queue_functions(functions)

  def queue_fixed(self, fixed_ref):
    print(fixed_models[fixed_ref])
    self.queue_functions(fixed_models[fixed_ref])

  def queue_functions(self, functions):
    #if isinstance(functions, list):
    #  functions = {i : {} for i in functions}

    for func in functions:
      self.pipeline_queue.append(func)

  def run(self, pgm, phrases, actions = None):
    
    
    if (actions is None) or (actions > len(self.pipeline_queue)):
      actions = len(self.pipeline_queue)

    for i in range(0, actions):
      print("Current action {0}/{1}: '{2}' running with parameters: {3:}".format(i+1,actions,self.pipeline_queue[i][0], str(self.pipeline_queue[i][1])))
      if self.trimmed:
        function_dict[self.pipeline_queue[i][0]](pgm, self.trimmed_phrases, **dict(self.pipeline_queue[i][1]))
      else:
        function_dict[self.pipeline_queue[i][0]](pgm, phrases, **dict(self.pipeline_queue[i][1]))

        if self.pipeline_queue[i][0] == "trimming":
          self.trimmed = True
          self.trimmed_phrases = function_dict[self.pipeline_queue[i][0]](pgm, phrases, **dict(self.pipeline_queue[i][1]))



      
    for i in range(0, actions):
      self.pipeline_queue.pop(0)

    




