"""# Import Libraries"""

# Install the latest Tensorflow version.
#pip3 install --quiet "tensorflow>=1.7"
# Install TF-Hub.
#pip3 install --quiet tensorflow-hub
#pip3 install --quiet seaborn

"""More detailed information about installing Tensorflow can be found at [https://www.tensorflow.org/install/](https://www.tensorflow.org/install/)."""

import tensorflow as tf
import tensorflow_hub as hub
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
import math
import io
import sys
import nlp_grouping_algorithm as grouping
import sys, getopt
import boto3
import json
import time
import grouping_manager
from print_analysis import *

def main(argv):

  environment = "project"
  version = "1.3"

  config_file = "none"
  input_file = "input.csv"
  output_file = "output"

  try:
    opts, args = getopt.getopt(argv,"c:i:e:o:",["config", "ifile","environment","ofile"])
  except getopt.GetoptError: 
    config_file = "default"
 
    
  for opt, arg in opts:
    if opt in ("-c","--config"):
      config_file = arg
    elif opt in ("-i", "--ifile"):
      input_file = arg
    elif opt in ("-v","--env"):
      environment = str(arg)
    elif opt in ("-o", "--ofile"):
      output_file = arg

  if environment!= "local":
    user_input = "yes"


  print("The current config is set to: {}".format(config_file))

  #print(environment)

  if (environment == "development") or (environment == "local") or (environment=="project") or (environment=="get vectors"):
    relative_path = os.path.abspath(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))
    input_path = os.path.join(relative_path,"data_sets", input_file)
    input_file = open(input_path,'r',encoding = 'utf-8', errors='replace')
    f = input_file.read() #decode(encoding='utf-8', errors = 'replace')

    dataframe = pd.read_csv(io.StringIO(f), engine='python', header = 0, keep_default_na=False)
    if environment == "project":
      grouping_manager.run("x", environment, config_file, dataframe, output_file)



if __name__ == '__main__':
  main(sys.argv[1:])