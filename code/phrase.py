from copy import deepcopy
class Phrase: 

  def __init__(self, t, v, p):
    self.text = t
    self.vector = v
    self.vector = v  
    self.passage_title = p
    self.other_data = {}
    self.group_id = -1
    
    self.group_quality = "unmarked"
    self.matching_confidence = 0

  def update_other_data(self, new_data):
    self.other_data = new_data
    
  def give_id(self, new_id):
    self.group_id = new_id

  def give_quality(self, new_qual):
    self.group_quality = new_qual
  def mark_grouped(self):
    self.grouped = True
  def mark_grouped(self):
    self.grouped = True
  
  def mark_ungrouped(self):
    self.grouped = False