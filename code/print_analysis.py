"""# Print Methods"""
# prints all the phrases the groups
from vector_math import *
def print_groups(groups):
  for i,group in enumerate(groups):
    print("Group number {}".format(i))
    print("Contains values:")
    for phrase in group.phrases:
      point_average = similarity(phrase.vector, group.average)
      print("-  ({0})   {1}   Intent: {2}".format(point_average, phrase.text, phrase.intent))

# it's in the name
def reset_groups(groups, phrases):
  for i, phrase in enumerate(phrases):
    phrase.mark_ungrouped()
  
  groups.clear()

#print out each intent and the phrases that matched to it
def print_intent_group(int_group):
  for key,value in int_group.items():
    print("Group '{}' contains".format(key))
   
    
    for i in value:
      print("-- {}".format(i.text))
      
# displays the recall and precision of each intent respective to each group
def print_analysis_group(created_groups):
  print("There are a total of {} new groups which have been created\n".format(len(created_groups)))
  
  total_phrases = 0
  
  #collect the total number of phrases in the data set
  for p in intent_group.values():
    total_phrases += len(p)
    
  
  
  
  for key,value in intent_group.items():
    print("Intent:  '{}'".format(key))
    
    unique_val = {}
    for p in value:
      unique_val[p.text] = False
    
    c_groups = []
    for g in created_groups:
      new_group = Group()
      new_group.phrases = list(dict.fromkeys(g.phrases))
      c_groups.append(new_group)
  
    

    for i,group in enumerate(c_groups):
      matching_phrases = 0
        
      for i_phrase in unique_val.keys():
        
        phrase_text = {}
        
        for phrase in group.phrases:
          
          phrase_text[phrase.text] = 0

        phrase_text = list(phrase_text)

          
        for text in phrase_text:
          if (text == i_phrase) and (unique_val[i_phrase] == False):
            matching_phrases += 1
            unique_val[i_phrase] = True
            

      if matching_phrases > 0:
        print("Group {} matched to this with: ".format(i))
        precision = matching_phrases/len(phrase_text)
          
        print("-- Precision: {0}  ({1}/{2}) ".format(round(precision,3), matching_phrases, len(phrase_text)))
          
        recall = matching_phrases/len(unique_val)
          
        print("-- Recall:    {0}  ({1}/{2}) ".format(round(recall,3), matching_phrases, len(unique_val)))
       
        
       
        
        
    print("")

