# -*- coding: utf-8 -*-
"""NLP Grouping Algorithm

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/15AAxr0c-cawl8XN8Hk2bFDpp-XN3fGoE

##### Copyright 2018 The TensorFlow Hub Authors.

Licensed under the Apache License, Version 2.0 (the "License");
"""

# Copyright 2018 The TensorFlow Hub Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

"""# Import Libraries"""

# Install the latest Tensorflow version.

"""More detailed information about installing Tensorflow can be found at [https://www.tensorflow.org/install/](https://www.tensorflow.org/install/)."""


import math
from group import Group
from random import shuffle
#from vector_math import *

from print_analysis import *

"""# Pipeline Methods"""

#1. Optimisation

# !!!!MAIN ALGORITHM!!!!

# Matches in a more unsupervised fashion
# params: list of groups, list of original input phrases, threshold to match to,
# match the best found/first found (bool), prioritise groups (bool)
# Print iterations progress
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total: 
        print()
def grouping_test(to_print):
  print(to_print)

def spherical_k_means(cluster_map, phrases_input, k=20, max_iterations=10):
  sim = similarity

  #initialise clusters
  for i in range(0,k):
    new_group = Group()
    new_group.generate_prototype()
    cluster_map.append(new_group)

  stable = False
  #iterate until max
  printProgressBar(0, max_iterations, prefix = 'Progress:', suffix = 'Max Iterations', length = 35)

  for i in range(0, max_iterations):
    for group in cluster_map:
      group.phrases.clear()
    printProgressBar(i+1, max_iterations, prefix = 'Progress:', suffix = 'Max Iterations', length = 35)
    #break if stable
    if stable:
      final_iteration = i

    for phrase in phrases_input:
      max(cluster_map, key=lambda g: sim(phrase.vector, g.prototype)).add_phrase(phrase)

    for group in cluster_map:
      group.update_centre()

    for group in cluster_map:
      if group.stable()==False:
        stable = False

  try:
    print("Terminated after: {} iterations".format(final_iteration))
  except:
    print("Terminated at max iterations ({})".format(max_iterations))

  for i, group in enumerate(cluster_map):
    group.assign_id(i)

  return cluster_map
#@profile
def match_average(pgm, phrases_input, threshold=0.1, best=True, group_priority=True):
    new_phrase_group_map = pgm
    phrases_input.reverse()
    
    local_sim = similarity

    printProgressBar(0, len(phrases_input), prefix = 'Progress:', suffix = 'Complete', length = 35)

    for i, i_phrase in enumerate(phrases_input):
      printProgressBar(i+1, len(phrases_input), prefix = 'Progress:', suffix = 'Complete', length = 35)
      if i_phrase.grouped==False:
          
        matched_group = None
        matched_phrase = None
        
        #print("ln: {}".format(len(new_phrase_group_map)))
        
        for j,group in enumerate(new_phrase_group_map):
          sim = local_sim(i_phrase.vector, group.prototype)
          #print("len of phrases is: ".format(len(group.phrases)))
          #print(len(i_phrase.vector))
          #print(len(group.prototype))
          #print(sim)
          if sim >= threshold:
            #print("test2")

            if best==True:
              #print(matched_group)
              if matched_group!=None:
                #print("similarity is:{}".format(local_sim(i_phrase.vector,new_phrase_group_map[matched_group].prototype)))
                if sim > local_sim(i_phrase.vector,new_phrase_group_map[matched_group].prototype):
                  
                  matched_group = j
                  #print("test group")

              else:
                matched_group = j
                #print("k")
                

            else:
              matched_group = j
              break

        for k, k_phrase in enumerate(phrases_input):
          if (k_phrase.grouped == False) and (k_phrase!=i_phrase):
            sim = local_sim(i_phrase.vector, k_phrase.vector)

            if sim >= threshold:
              if best:
                if matched_phrase!=None:
                  if sim > local_sim(i_phrase.vector, phrases_input[matched_phrase].vector):
                    matched_phrase = k
                    #print("test phrase")

                else:
                  matched_phrase = k
              else:
                matched_phrase = k
                break

        if (matched_group==matched_phrase==None):
          i_phrase.mark_grouped()
          new_group = Group()
          new_group.add_phrase(i_phrase)
          new_group.update_centre()
          new_phrase_group_map.append(new_group)


        elif (matched_group!=None) and (matched_phrase==None):
          npgm_group = new_phrase_group_map[matched_group]
          i_phrase.mark_grouped()
          npgm_group.add_phrase(i_phrase)
          npgm_group.update_centre()

        elif (matched_group==None) and (matched_phrase!=None):
          i_phrase.mark_grouped()
          phrases_input[matched_phrase].mark_grouped()
          new_group = Group()
          new_group.add_phrase(i_phrase)
          new_group.add_phrase(phrases_input[matched_phrase])
          new_group.update_centre()
          #print("group length: {}".format(len(new_phrase_group_map)))
          new_phrase_group_map.append(new_group)

        #if (matched_group!=None) and (matched_phrase!=None):
          #print("comparison triggered")
        else:
          npgm_group = new_phrase_group_map[matched_group]
          if group_priority==False:
            if local_sim(i_phrase.vector, phrases_input[matched_phrase].vector) > local_sim(i_phrase.vector, npgm_group.average):
              i_phrase.mark_grouped()
              phrases_input[matched_phrase].mark_grouped()
              new_group = Group()
              new_group.add_phrase(i_phrase)
              new_group.add_phrase(phrases_input[matched_phrase])
              new_group.update_centre()
              new_phrase_group_map.append(new_group)

            else:
              i_phrase.mark_grouped()
              npgm_group.add_phrase(i_phrase)
              npgm_group.update_centre()
              
          else:
            i_phrase.mark_grouped()
            npgm_group.add_phrase(i_phrase)
            npgm_group.update_centre()
            

            
    pgm = new_phrase_group_map
    return new_phrase_group_map

# Likely Redundant

# iterate accross all the phrase vectors and create groups with the initial phrase
# automatically set as the "average"
def match_first(theshold):
    new_phrase_group_map = []
    for i, i_phrase in enumerate(phrases):

      #if the phrase doesn't belong to a group yet, create a new group for it
      # (the first phrase will always create it's own new group)
      #print (i_phrase.grouped)
      if i_phrase.grouped is False:
        #print(i_phrase.text)
        #groups are dictionaries of key phrases to vector values
        new_group = Group()
        i_phrase.mark_grouped()

        new_group.add_phrase(i_phrase)


        #iterate accross every following vector
        for j, j_phrase in enumerate(phrases):

          #if the following vectors are not in a group and have similarity above
          #the threshold, add it to the group and mark it as grouped
          if j_phrase.grouped is False:
            if local_sim(i_phrase.vector,j_phrase.vector)>=t:
              j_phrase.mark_grouped()
              new_group.add_phrase(j_phrase)



        new_phrase_group_map.append(new_group)
        
    return new_phrase_group_map

#gets rid of small or large phrases. minimum is set by the user
def trimming(pgm, phrases, minimum=0, maximum=10):
  new_list = []
  for phrase in phrases:
    if (phrase.text.count(" ") >= minimum) and (phrase.text.count(" ") < maximum):
      new_list.append(phrase)
  phrases = new_list
  return new_list

# gets rid of groups that are too small or large
# separated because 
def large_group_processing(phrase_group_map, phrases, max_size):
  ungroup = []
  for group in phrase_group_map:
    if (len(group.phrases) > max_size):
      for i, phrase in enumerate(group.phrases):
        
        phrases[phrases.index(phrase)].mark_ungrouped()
        
      ungroup.append(group)
  
  for group in ungroup:
    phrase_group_map.remove(group)
    
def small_group_processing(phrase_group_map, phrases, min_size):
  ungroup = []
  for group in phrase_group_map:
    if (len(group.phrases) > group_size):
      for i, phrase in enumerate(group.phrases):

        phrases[phrases.index(phrase)].mark_ungrouped()

      ungroup.append(group)

  for group in ungroup:
    phrase_group_map.remove(group)

def try_split(group, new_threshold):
  valid_split = True
  new_groups = []
  
  for phrase in group.phrases:
    phrase.mark_ungrouped()
    
  new_groups = match_average(new_groups, trimmed_phrases, new_threshold, True, True)
  
  #update this to be based on max size?
  if len(new_groups) >= 5:
    valid_split = False
  
  
  return (valid_split, new_groups)

# Attempts to split groups that are too large. "new_t" is the new threshold the groups will try rematch from
# Splits will not occur if the resulting groups aren't good and return the last valid set of groups
# NOTE: Try and use group quality for this?
def group_splitting(phrase_group_map, phrases, max_size, new_t):
  remove_map = []
  groups_to_add = []
  
  learning_rate = 0.01
  for i, group in enumerate(phrase_group_map):
    
    local_threshold = new_t
    
    if len(group.phrases) > max_size:
      replace = False
      t_split = try_split(group, local_threshold)
      
      if t_split[0]:
        replace = True
        remove_map.append(group)
        
        while t_split[0] == True:
          print(local_threshold)
          split_result = t_split[1]
          local_threshold+=learning_rate
          t_split = try_split(group, local_threshold)
          
        if replace:
          groups_to_add.extend(split_result)
      
  for group in remove_map:
      phrase_group_map.remove(group)
  
  phrase_group_map.extend(groups_to_add)

def merge_groups(group1, group2):
  new_group = Group()
  for phrase1 in group1.phrases:
    new_group.add_phrase(phrase1)
  
  for phrase2 in group2.phrases:
    new_group.add_phrase(phrase2)
    
  new_group.update_centre()
  
  return new_group



# Attempt to expand smaller groups to pick up smaller ones that may have a high overlap
# Weight determines how much "worse" the group is allowed to get when picking up new groups
# WARNING: Currently extremely slow and in need of optimisation. Large iterations aren't recommended at this stage

#1. Optimisation
def group_realignment(groups, weight):
  remerged_groups= []
  checked_map = [False]*len(groups)
  
  for i, i_group in enumerate(groups):

    for j, j_group in enumerate(groups):
      if (checked_map[j] == False) and (checked_map[i] == False):
        if (i is not j):
          i_lim = i_group.get_limit()
          if i_lim > 1:
            i_lim = 1

          j_lim = j_group.get_limit()
          if j_lim > 1:
            j_lim = 1
          
          sim = similarity(i_group.average, j_group.average)
          if sim > 1:
            sim = 1


          alpha = math.acos(i_lim)
          beta = math.acos(j_lim)
          l = math.acos(sim)
     
          if (alpha + l) < (weight*beta):
            remerged_groups.append(merge_groups(i_group, j_group))
            checked_map[i] = True
            checked_map[j] = True
            
          if (beta + l) < (weight*alpha):
            remerged_groups.append(merge_groups(i_group, j_group))
            checked_map[i] = True
            checked_map[j] = True
            
    
    if checked_map[i] == False:
      checked_map[i] = True
      remerged_groups.append(i_group)
          
        
  return remerged_groups
  