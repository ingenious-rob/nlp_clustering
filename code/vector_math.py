import math
import numpy
# Vector math
def vector_magnitude( vector ):
  magnitude = 0;
  for x in vector:
    magnitude += (x*x)
  
  magnitude = math.sqrt(magnitude)
  
  return magnitude

#@profile
def dot_product(vector_1, vector_2):
  dot = 0;
  
  end = int(len(vector_1))

  if (len(vector_1) == len(vector_2)):
    for i in range(0, end):     
      dot += vector_1[i]*vector_2[i]
  return dot

def normalise(vector):
  result = []
  mag = vector_magnitude(vector)
  for x in vector:
    result.append(x/mag)
  return result
  
#@profile   
def similarity(vector_1, vector_2):
  cosine_similarity = numpy.dot(vector_1,vector_2)
  return cosine_similarity

def euc_distance(vector_1, vector_2):
  result = 0

  size = int(len(vector_1))

  if (len(vector_2) == size):
    a = numpy.array(vector_1)
    b = numpy.array(vector_2)
    result = numpy.linalg.norm(a-b)
  else:
    result = -1

  return result